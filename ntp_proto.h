#ifndef _NTP_PROTO_H_INCLUDED_
#define _NTP_PROTO_H_INCLUDED_

#include <stdint.h>

static const unsigned short cNTP_PORT = 123;
static const unsigned char cNTP_VERSION = 4;
// pocet vterin mezi 1900 a 1970 (70 let, 17 prestupnych let)
static const unsigned cNTP_OFFSET = (70*365 + 17)*24*60*60U;


/*
 * Fixed Point format
 * Prvni slovo obsahuje vteriny od zacatku epochy
 * Druhe slovo je zlomek vteriny, UINT32_MAX je jedna vterina
 */
typedef struct {
	uint32_t i;
	uint32_t f;
} l_fp;

/*
 * NTP Packet structure
 */
typedef struct {
	uint8_t li_vn_mode;
	uint8_t stratum;
	uint8_t ppoll;
	int8_t precision;
	uint32_t rootdelay;
	uint32_t rootdisp;
	uint32_t refid;
	l_fp reftime;
	l_fp origin;
	l_fp received;
	l_fp transmitted;
} ntp_packet;


#endif
