#include "ntp.h"
#include "ntp_proto.h"

#include <time.h>
#include <stdio.h>
#include <string.h> //memset
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>  // getaddrinfo
#include <unistd.h> // close
#include <fcntl.h>
#include <poll.h>

static int verbose_flag = 0;
static struct addrinfo ai_hints;
static int fd;

#define DEBUG(...) { if (verbose_flag) printf("NTP::" __VA_ARGS__); }
#define DEBUG_TIME(str, x) DEBUG("%s = %ld.%09ld\n", str, (x).tv_sec, (x).tv_nsec)
#define ERROR(x) { if (errno) DEBUG("ERR(%s) %s", (x), strerror(errno)); }

void _average_timespec(struct timespec *res, const struct timespec *a1, const struct timespec *a2);

static char *_hostport(const struct sockaddr *x) {
	static char buf[100];
	char *p = buf;
	if (!x)
		sprintf(buf, "sockadd == NULL");
	else {
		int i;
		for (i = 2; i < 6; i++) {
			p += sprintf(p, "%u", x->sa_data[i] & 0xff);
			if (i < 5)
				*p++ = '.';
		}
		i = ((x->sa_data[0] & 0xff) << 8) | (x->sa_data[1] & 0xff);
		sprintf(p, ":%d", i);
	}
	return buf;
}

static void _print_buffer(uint8_t *buf, int len) {
	int i, j;
	for (i = j = 0; j < len; j += 8) {
		printf("NTP : %04X : ", j);
		for (i = 0; i + j < len && i < 8 ; i++) {
			printf("%02X ", buf[i+j]);
		}
		printf("\n");
	}
}

static int _resolve_dns_record(const char *name) {
	memset(&ai_hints, 0, sizeof(struct addrinfo));
	ai_hints.ai_family = AF_INET;
	ai_hints.ai_socktype = SOCK_DGRAM;
	struct addrinfo *res;
	int s;
	// doporucovany zpusob DNS resolve
	s = getaddrinfo(name, "ntp", &ai_hints, &res);
	if (s) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		return 0;
	}
	ERROR("dns");
	struct addrinfo *rp;
	// iteruji vsemi vysledky DNS resolve
	for (rp = res; rp; rp = rp->ai_next) {
		if (fd <= 0) {
			// jeste se nepovedlo spojit
			fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
			if (fd == -1) {
				DEBUG("resolved address %s failed to create socket\n", _hostport(rp->ai_addr));
				ERROR("socket");
				continue;
			}
			if (connect(fd, rp->ai_addr, rp->ai_addrlen) == -1) {
				DEBUG("resolved address %s failed to connect\n", _hostport(rp->ai_addr));
				ERROR("connect");
				close(fd);
				fd = 0;
			} else {
				DEBUG("resolved address %s connected with file descriptor %d\n", _hostport(rp->ai_addr), fd);
			}
		} else
			DEBUG("resolved address %s skipped\n", _hostport(rp->ai_addr));
	}
	freeaddrinfo(res);
	return 0;
}

#define SAVE_TIME(x) clock_gettime(CLOCK_REALTIME_COARSE, &(x))

int ntp_query_server(const char *name, ntp_info *out, unsigned timeout) {

	if (_resolve_dns_record(name) || fd <= 0)
		return 1;

	ntp_packet pkt;
	ssize_t nr;
	int ret = 0;

	memset(&pkt, 0, sizeof(pkt));
	pkt.li_vn_mode = 0x1b;

	SAVE_TIME(out->lbegin);

	// zapis v blokujicim rezimu
	if (write(fd, &pkt, sizeof(pkt)) != sizeof(pkt)) {
		DEBUG("incomplete packet transmitted\n");
		ERROR("write");
		ret = 2;
	} else {
		DEBUG("formed packet sent\n");

		// cteni bude v neblokujicim
		fcntl(fd, F_SETFL, O_NONBLOCK | fcntl(fd, F_GETFL, 0));

		struct pollfd pfd = {fd, POLLIN, 0};
		// while osetruje nechteny interrupt
		while ((nr = poll(&pfd, 1, timeout)) == -1 && errno == EINTR);
		// poll vrati prave 1, pokud je 1 descriptor zraly ke cteni
		if (nr == 1) {
			if ((nr = read(fd, &pkt, sizeof(pkt))) == -1) {
				DEBUG("cannot read response\n");
				ERROR("read");
				ret = 3;
			} else {
				SAVE_TIME(out->lend);
				DEBUG("received %zd bytes as answer\n", nr);
				if (verbose_flag)
					_print_buffer((void *)&pkt, sizeof(pkt));
				DEBUG_TIME("local begin", out->lbegin);
				DEBUG_TIME("local end", out->lend);
				_average_timespec(&out->lavg, &out->lbegin, &out->lend);
				DEBUG_TIME("local avg", out->lavg);
				out->rbegin.tv_sec = ntohl(pkt.received.i) - cNTP_OFFSET;
				out->rbegin.tv_nsec = ntohl(pkt.received.f) * 1000000000UL / UINT32_MAX;
				DEBUG_TIME("remote begin", out->rbegin);
				out->rend.tv_sec = ntohl(pkt.transmitted.i) - cNTP_OFFSET;
				out->rend.tv_nsec = ntohl(pkt.transmitted.f) * 1000000000UL / UINT32_MAX;
				DEBUG_TIME("remote end", out->rend);
				_average_timespec(&out->ravg, &out->rbegin, &out->rend);
				DEBUG_TIME("remote avg", out->ravg);
			}
		} else {
			DEBUG("timeout %d ms occured\n", timeout);
			ret = 4;
		}
	}
	close(fd);
	return ret;
}

void ntp_set_verbose_level(int x) {
	verbose_flag = x;
	DEBUG("verbose flag changed to %d\n", x);
}

#if 0
static int _get_zone_shift(void) {
	time_t t;
	t = time(0);
	struct tm *p = localtime(&t);
	return (int)p->tm_gmtoff;
}
#endif

static const long cBILION = 1000000000L;

void ntp_add_timespec(struct timespec *res, const struct timespec *what) {
	res->tv_sec += what->tv_sec;
	res->tv_nsec += what->tv_nsec;
	while (res->tv_nsec >= cBILION) {
		res->tv_nsec -= cBILION;
		res->tv_sec += 1;
	}
}

void ntp_sub_timespec(struct timespec *res, const struct timespec *what) {
	res->tv_sec -= what->tv_sec;
	while (res->tv_nsec < what->tv_nsec) {
		res->tv_nsec += cBILION;
		res->tv_sec -= 1;
	}
	res->tv_nsec -= what->tv_nsec;
}

void ntp_halve_timespec(struct timespec *res) {
	if (res->tv_sec & 1) {
		res->tv_sec -= 1;
		res->tv_nsec += cBILION;
	}
	res->tv_sec /= 2;
	res->tv_nsec /= 2;
}

void _average_timespec(struct timespec *res, const struct timespec *a1, const struct timespec *a2) {
	res->tv_sec = a2->tv_sec;
	res->tv_nsec = a2->tv_nsec;
	ntp_sub_timespec(res, a1);
	ntp_halve_timespec(res);
	ntp_add_timespec(res, a1);
}
