#include <stdio.h>
#include <unistd.h>
#include "ntp.h"


int main(int argc, char *argv[]) {
	int opt;
	char *ntp_ip = "0.cz.pool.ntp.org";

	while ((opt = getopt(argc, argv, "v")) != -1) {
		switch (opt) {
		case 'v':
			ntp_set_verbose_level(1);
			break;
		default:
			fprintf(stderr, "Usage: %s [-v] ntp_server_address\n", argv[0]);
			return -1;
		}
	}

	if (optind < argc) {
		// vezmu hostname z prikazove radky
		ntp_ip = argv[optind];
	}

	ntp_info i;
	// timeout nastavim na 200ms
	if (!ntp_query_server(ntp_ip, &i, 200)) {
		printf("Got time: %s", asctime(gmtime(&(i.rend.tv_sec))));
		printf("%ld.%09lu\n", i.rend.tv_sec, i.rend.tv_nsec);
	} else
		printf("Got error..\n");

	return 0;
}
